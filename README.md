# Linux for PSI WaveDAQ Data Concentrator Board (DCB)

## Building U-Boot, Kernel and Rootfilesystem

If all prerequisites (see below for installation instructions) are fulfilled, building is quite easy but
takes up to several hours and needs > 100GiByte free disk space.

Clone main repository and submodules with bitbake layers and yocto environment:

```
$ cd <working directory with >100 GiByte free space>
$ git clone git@bitbucket.org:twavedaq/wavedaq_linux.git
$ cd wavedaq_linux
$ git submodule update --init --recursive
```

adjust device tree for u-boot and kernel, if required

```
./layer/meta-psi-wavedaq/meta-wavedaq-dcb-bsp/recipes-bsp/u-boot/files/git/arch/arm/dts/zynq_psi_wavedaq_dcb.dts
./layer/meta-psi-wavedaq/meta-wavedaq-dcb-bsp/recipes-kernel/linux/files/git/arch/arm/boot/dts/zynq_psi_wavedaq_dcb.dts
```

Build required targets:

```
$ cd system/dcb_rootfs_lsb
$ . init_env.sh
$ bitbake u-boot-xlnx
$ bitbake linux-xlnx
$ bitbake core-image-lsb-sdk
```

### Important output files:

```
./deploy/images/zynq_psi_wavedaq_dcb/u-boot.elf
./deploy/images/zynq_psi_wavedaq_dcb/zynq_psi_wavedaq_dcb.dtb
./deploy/images/zynq_psi_wavedaq_dcb/uImage
./deploy/images/zynq_psi_wavedaq_dcb/core-image-lsb-sdk-zynq_psi_wavedaq_dcb.tar.gz
```

Optionaly tweak rootfs as required, e.g. see [rootfs_adjusts.md](rootfs_adjusts.md)
Extract rootfs image to SD-Card or export on NFS server.

For building a Crosscompiler environment:

```
$ bitbake core-image-lsb-sdk -c do_populate_sdk
```

generates a quite large executable installer file:

```
./deploy/sdk/poky-lsb-glibc-x86_64-core-image-lsb-sdk-cortexa9t2hf-neon-zynq_psi_wavedaq_dcb-toolchain-2.7.sh
```

Prerequisites
=============
- Yocto build environment

Yocto build environment
-----------------------

You should use a supported Linux distribution for your build host system. See

https://www.yoctoproject.org/docs/2.7/ref-manual/ref-manual.html#detailed-supported-distros

On PSI RH7 installation the epel repository should already be installed, so skip 'yum install -y epel-release' and 'yum makecache' and just install:

```
$ sudo yum install python36-pip python36 python36-libs python36-setuptools

$ sudo yum install gawk make wget tar bzip2 gzip unzip perl patch \
    diffutils diffstat git cpp gcc gcc-c++ glibc-devel texinfo chrpath socat \
    perl-Data-Dumper perl-Text-ParseWords perl-Thread-Queue xz \
    which SDL-devel xterm

$ sudo pip3 install GitPython jinja2
```

for building Yocto Project documentation manuals also install:

```
$ sudo yum install docbook-style-dsssl docbook-style-xsl \
    docbook-dtds docbook-utils fop libxslt dblatex xmlto
```


Submodules
==========

Relative to the working directory, the repository submodules reside in the following paths:

| Repository              | Path                    | Comment                                         |
|-------------------------|-------------------------|-------------------------------------------------|
| wavedaq_linux           | (root of linux repos)   | Linux main repo containing the other submodules |
| wavedaq_yocto_build_env | system/dcb_rootfs_lsb   | Custom Main Yocto Configuration                 |
| wavedaq_yocto_layer     | layer/meta-psi-wavedaq  | Custom Layer                                    |
| Poky                    | layer/poky              | Yocto (Reference Distribution)                  |
| OpenEmbedded            | layer/meta-openembedded | Yocto (Build System)                            |
