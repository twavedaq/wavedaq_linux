# U-Boot environment Data Concentrator Board (DCB)

## Environment Template (NFS boot)

```
autoload=no
boot_targets=mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc
bootargs=console=ttyPS0,115200 earlycon root=/dev/mmcblk0p2 rootfstype=ext4 rw
bootcmd=run cmd_marvell_init; run cmd_boot_linux
bootdelay=6
bootfile=pxelinux.0
cfg_bootargs_common=console=ttyPS0,115200 earlycon rootwait
cfg_fdt_addr=0x2000000
cfg_ip=dhcp
cfg_kernel_addr=0x2080000
cfg_kernel_load=nfs
cfg_mmc_dev=mmc 0:2
cfg_mmc_fdt_image=/boot/zynq_psi_wavedaq_dcb.dtb
cfg_mmc_kernel_image=/boot/uImage
cfg_nfs_fdt_image=rootfs/boot/zynq_psi_wavedaq_dcb.dtb
cfg_nfs_kernel_image=rootfs/boot/uImage
cfg_nfs_rootfs_path=rootfs
cfg_nfs_server_path=/data/export
cfg_nfs_serverip=129.129.188.245
cfg_rootfs=nfs
cfg_rootfs_flags=rw
cmd_boot_linux=run cmd_kernel_load; run set_bootargs; bootm $cfg_kernel_addr - $cfg_fdt_addr
cmd_ipconfig=dhcp
cmd_kernel_load=run cmd_kernel_load_${cfg_kernel_load}
cmd_kernel_load_mmc=load ${cfg_mmc_dev} ${cfg_kernel_addr} $cfg_mmc_kernel_image; load ${cfg_mmc_dev} ${cfg_fdt_addr} ${cfg_mmc_fdt_image}
cmd_kernel_load_nfs=run cmd_ipconfig; setenv serverip $cfg_nfs_serverip; nfs $cfg_kernel_addr $cfg_nfs_server_path/$cfg_nfs_kernel_image; nfs $cfg_fdt_addr $cfg_nfs_server_path/$cfg_nfs_fdt_image
cmd_marvell_init=i2c dev 0; i2c mw 56 1d 00; i2c mw 56 1d 06; i2c mw 56 1e 40; i2c mw 56 1e 00; i2c mw 56 1d 00; i2c mw 56 1d 0A; i2c mw 56 1e 82; i2c mw 56 1e 00; i2c mw 56 0 81 ; i2c mw 56 0 40 ; i2c mw 56 16 00; i2c mw 56 16 01; i2c mw 56 0 91 ; i2c mw 56 0 40 ; i2c mw 56 16 00; i2c mw 56 16 00; i2c mw 56 1b 90; i2c mw 56 1b 84; i2c mw 56 9 0f ; i2c mw 56 9 00 ; i2c mw 56 0 81 ; i2c mw 56 0 40 ; i2c mw 56 4 0d ; i2c mw 56 4 e1 ; i2c mw 56 0 91 ; i2c mw 56 0 40
dnsip=129.129.190.11
eth1addr=00:50:C2:46:D5:01
ethact=ethernet@e000b000
ethaddr=00:50:C2:46:D5:00
fdtcontroladdr=3ffb7820
fileaddr=2000000
filesize=233
gatewayip=129.129.193.1
gatewazip=129.129.193.1
ipaddr=129.129.193.162
modeboot=sdboot
netmask=255.255.255.0
serverip=129.129.188.245
set_bootargs=run set_ipargs; run set_rootfsargs; setenv bootargs ${cfg_bootargs_common} ${tmp_rootfsargs} ${tmp_ipargs}
set_ipargs=run set_ipargs_${cfg_ip}
set_ipargs_dhcp=setenv tmp_ipargs ip=::::${hostname}:eth0:dhcp
set_ipargs_none=setenv tmp_ipargs
set_ipargs_static=setenv tmp_ipargs ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:eth0:off
set_rootfsargs=run set_rootfsargs_${cfg_rootfs}
set_rootfsargs_mmc=setenv tmp_rootfsargs root=/dev/mmcblk0p2 rootfstype=ext4 ${cfg_rootfs_flags}
set_rootfsargs_nfs=setenv tmp_rootfsargs root=/dev/nfs nfsroot=${cfg_nfs_serverip}:${cfg_nfs_server_path}/${cfg_nfs_rootfs_path},nfsvers=3 ${cfg_rootfs_flags}
stderr=serial@e0001000
stdin=serial@e0001000
stdout=serial@e0001000
tmp_rootfsargs=root=/dev/mmcblk0p2 rootfstype=ext4 rw
```

Commands for setting environment template variables

```
setenv autoload 'no'
setenv boot_targets 'mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc mmc'
setenv bootargs 'console=ttyPS0,115200 earlycon root=/dev/mmcblk0p2 rootfstype=ext4 rw'
setenv bootcmd 'run cmd_marvell_init; run cmd_boot_linux'
setenv bootdelay '6'
setenv bootfile 'pxelinux.0'
setenv cfg_bootargs_common 'console=ttyPS0,115200 earlycon rootwait'
setenv cfg_fdt_addr '0x2000000'
setenv cfg_ip 'dhcp'
setenv cfg_kernel_addr '0x2080000'
setenv cfg_kernel_load 'nfs'
setenv cfg_mmc_dev 'mmc 0:2'
setenv cfg_mmc_fdt_image '/boot/zynq_psi_wavedaq_dcb.dtb'
setenv cfg_mmc_kernel_image '/boot/uImage'
setenv cfg_nfs_fdt_image 'rootfs/boot/zynq_psi_wavedaq_dcb.dtb'
setenv cfg_nfs_kernel_image 'rootfs/boot/uImage'
setenv cfg_nfs_rootfs_path 'rootfs'
setenv cfg_nfs_server_path '/data/export'
setenv cfg_nfs_serverip '129.129.188.245'
setenv cfg_rootfs 'nfs'
setenv cfg_rootfs_flags 'rw'
setenv cmd_boot_linux 'run cmd_kernel_load; run set_bootargs; bootm $cfg_kernel_addr - $cfg_fdt_addr'
setenv cmd_ipconfig 'dhcp'
setenv cmd_kernel_load 'run cmd_kernel_load_${cfg_kernel_load}'
setenv cmd_kernel_load_mmc 'load ${cfg_mmc_dev} ${cfg_kernel_addr} $cfg_mmc_kernel_image; load ${cfg_mmc_dev} ${cfg_fdt_addr} ${cfg_mmc_fdt_image}'
setenv cmd_kernel_load_nfs 'run cmd_ipconfig; setenv serverip $cfg_nfs_serverip; nfs $cfg_kernel_addr $cfg_nfs_server_path/$cfg_nfs_kernel_image; nfs $cfg_fdt_addr $cfg_nfs_server_path/$cfg_nfs_fdt_image'
setenv cmd_marvell_init 'i2c dev 0; i2c mw 56 1d 00; i2c mw 56 1d 06; i2c mw 56 1e 40; i2c mw 56 1e 00; i2c mw 56 1d 00; i2c mw 56 1d 0A; i2c mw 56 1e 82; i2c mw 56 1e 00; i2c mw 56 0 81 ; i2c mw 56 0 40 ; i2c mw 56 16 00; i2c mw 56 16 01; i2c mw 56 0 91 ; i2c mw 56 0 40 ; i2c mw 56 16 00; i2c mw 56 16 00; i2c mw 56 1b 90; i2c mw 56 1b 84; i2c mw 56 9 0f ; i2c mw 56 9 00 ; i2c mw 56 0 81 ; i2c mw 56 0 40 ; i2c mw 56 4 0d ; i2c mw 56 4 e1 ; i2c mw 56 0 91 ; i2c mw 56 0 40'
setenv dnsip '129.129.190.11'
setenv eth1addr '00:50:C2:46:D5:01'
setenv ethact 'ethernet@e000b000'
setenv ethaddr '00:50:C2:46:D5:00'
setenv fdtcontroladdr '3ffb7820'
setenv fileaddr '2000000'
setenv filesize '233'
setenv gatewayip '129.129.193.1'
setenv gatewazip '129.129.193.1'
setenv ipaddr '129.129.193.162'
setenv modeboot 'sdboot'
setenv netmask '255.255.255.0'
setenv serverip '129.129.188.245'
setenv set_bootargs 'run set_ipargs; run set_rootfsargs; setenv bootargs ${cfg_bootargs_common} ${tmp_rootfsargs} ${tmp_ipargs}'
setenv set_ipargs 'run set_ipargs_${cfg_ip}'
setenv set_ipargs_dhcp 'setenv tmp_ipargs ip=::::${hostname}:eth0:dhcp'
setenv set_ipargs_none 'setenv tmp_ipargs'
setenv set_ipargs_static 'setenv tmp_ipargs ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:eth0:off'
setenv set_rootfsargs 'run set_rootfsargs_${cfg_rootfs}'
setenv set_rootfsargs_mmc 'setenv tmp_rootfsargs root=/dev/mmcblk0p2 rootfstype=ext4 ${cfg_rootfs_flags}'
setenv set_rootfsargs_nfs 'setenv tmp_rootfsargs root=/dev/nfs nfsroot=${cfg_nfs_serverip}:${cfg_nfs_server_path}/${cfg_nfs_rootfs_path},nfsvers=3 ${cfg_rootfs_flags}'
setenv stderr 'serial@e0001000'
setenv stdin 'serial@e0001000'
setenv stdout 'serial@e0001000'
setenv tmp_rootfsargs 'root=/dev/mmcblk0p2 rootfstype=ext4 rw'
```

## Dumping and Restoring U-Boot Environments

### Clean whole Environment

```
$ env import -d -b 0 0
```

### Configure NFS Boot

```
$ setenv cfg_kernel_load nfs
$ setenv cfg_rootfs nfs
$ setenv cfg_ip dhcp
$ saveenv
```


### Configure MMC (SD Card) Boot without Network

```
$ setenv cfg_kernel_load mmc
$ setenv cfg_rootfs mmc
$ setenv cfg_ip none
$ saveenv
```


### Export to Textfile on SD Card

```
$ env export -t 0x1f00000
$ fatwrite mmc 0:1 0x1f00000 uEnv.txt ${filesize}
```


### Import from Textfile on SD Card

```
$ fatload mmc 0:1 0x1f00000 uEnv.txt
$ env import -d -t 0x1f00000 ${filesize}
$ saveenv
```


## DCB Specific Initialization

### Initializing Hostname, Cratename and MAC addresses

To simplify DCB commissioning, a set of specific environment variables can be set with a command, specifically
implemented for the DCB.

```
$ dcbcfg -s <dcb_number>
```
The variables that are set by this command all depend on the serial number (e.g. 7):

| Variable  | Value             | Usage Remarks                      |
|-----------|-------------------|------------------------------------|
| hostname  | dcb07             | Network configuration (DHCP)       |
| cratename | dcb07_crate       | Configuration for DCBS application |
| ethaddr   | 00:50:C2:46:D5:0E | Network configuration of SFP 0     |
| eth1addr  | 00:50:C2:46:D5:0F | Network configuration of SFP 1     |

Note that the first part 00:50:C2:46:D5:__ of the MAC address is constant.
These values represent an initial setup that allows the system to be operated.
However they might still be changed.
MAC addresses have to be chosen with care in order to avoid collisions in the network.
Do not use unregisterd or even random values.

### Load Environment from File

If environment variables should be imported from a text file on the sd-card, it can be done with the same command:

```
$ dcbcfg -i <file_name>
```


Of course loading and initialization can also be combined:

```
$ dcbcfg -s <dcb_number> -i <file_name>
```
The variables copied from the file are completed or overriden by -s.
