#!/bin/sh

# Default source directories
srcdir_boot="ems-rh7-lc:/data/export/rootfs/boot/prod"
srcdir_rootfs="ems-rh7-lc:/data/export/rootfs"
srcdir_home="ems-rh7-lc:/data/export/home"
srcdir_fw_sw="ems-rh7-lc:/data/export/fw_sw"
# Default Device
sdcard_dev=/dev/mmcblk0
# Default mount directory
mnt_dir=/run/media
# Target partition table
part_table=/home/root/sd_card_partition_table

# Warning: unqualified usage might corrupt the system setup
printf "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
printf "Warning: This script changes the partition table of the DCB SD-card.\n"
printf "This can lead to a corrupted system setup and prevent the system\n"
printf "from booting properly (e.g. in case the partitioning is applied to\n"
printf "the wrong device).\n"
printf "\nThe script is programmed to run on the DCB.\n"
printf "\nAll information currently stored on the SD-Card is lost.\n"
printf "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
printf "\nDo you want to continue y/[n]: "
read input
if [ "$input" != "y" ] && [ "$input" != "yes" ];
then
  printf "Terminating script"
  return
fi

# Ask four SD-card device
printf "\nSD-Card device [$sdcard_dev]: "
read input
if [ $input ];
then
  sdcard_dev=$input
fi

# Check how many partitions are already configured
sdcard_partitions=$(grep -c 'mmcblk0p[0-9]' /proc/partitions)
printf "Partitions found on SD-Card: $sdcard_partitions\n"

# Ask four source directories
printf "\nConfigure source directories to copy sd-card content from\n"
# Boot
printf "Boot directory (containing BOOT.bin, .dtb, kernel image) [$srcdir_boot]: "
read input
if [ $input ];
then
  srcdir_boot=$input
fi

# Rootfs
printf "Rootfs directory [$srcdir_rootfs]: "
read input
if [ $input ];
then
  srcdir_rootfs=$input
fi

printf "Home directory [$srcdir_home]: "
read input
if [ $input ];
then
  srcdir_home=$input
fi

printf "Firm- and software (fw_sw) directory [$srcdir_fw_sw]: "
read input
if [ $input ];
then
  srcdir_fw_sw=$input
fi

printf "\n\nSummary:\n"
printf "Boot   source dir: $srcdir_boot\n"
printf "Rootfs source dir: $srcdir_rootfs\n"
printf "Home   source dir: $srcdir_home\n"
printf "Fw_sw  source dir: $srcdir_fw_sw\n"

printf "Is this correct y/[n]? "
read input
if [ "$input" != "y" ] && [ "$input" != "yes" ];
then
  printf "Terminating script"
  return
fi

# Unmount partitions
printf "\nUnmounting partitions\n"
for i in `seq 1 $sdcard_partitions`
#for (( i=0; i <= sdcard_partitions; i++ ));
do
  mnt_dev="$sdcard_dev"
  mnt_dev+="p"
  mnt_dev+="$i"
  mnt_point=$(findmnt -nr -o target -S $mnt_dev)
  if [ $mnt_point ];
  then
    printf "Unmounting device $mnt_dev from mount point $mnt_point ..."
    umount $mnt_point
    printf "done\n"
  fi
done

# Load predefined partition table
printf "\nLoading partition table\n"
sfdisk $sdcard_dev < $part_table
partprobe $sdcard_dev

# Configure new partitions
printf "\nConfigure file system on partitions\n"
mkfs.vfat /dev/mmcblk0p1
mkfs.ext4 /dev/mmcblk0p2
mkfs.ext4 /dev/mmcblk0p3
mkfs.ext4 /dev/mmcblk0p4

# Mount new partitions
printf "\nMount new partitions\n"
mkdir "$mnt_dir/mmcblk0p1"
mkdir "$mnt_dir/mmcblk0p2"
mkdir "$mnt_dir/mmcblk0p3"
mkdir "$mnt_dir/mmcblk0p4"
mount /dev/mmcblk0p1 "$mnt_dir/mmcblk0p1"
mount /dev/mmcblk0p2 "$mnt_dir/mmcblk0p2"
mount /dev/mmcblk0p3 "$mnt_dir/mmcblk0p3"
mount /dev/mmcblk0p4 "$mnt_dir/mmcblk0p4"

# Copy content to partitions
printf "\nCopy data to partitions\n"
mkdir "$mnt_dir/boot"
mkdir "$mnt_dir/rtfs"
mkdir "$mnt_dir/home"
mkdir "$mnt_dir/fwsw"

printf "\nCopying $srcdir_boot ---> $mnt_dir/mmcblk0p1 ..."
rm -rf $mnt_dir/mmcblk0p1/*
mount -t nfs -o nolock,vers=3,ro $srcdir_boot "$mnt_dir/boot"
cp $mnt_dir/boot/* $mnt_dir/mmcblk0p1
sync
umount $mnt_dir/boot
printf " done\n"

printf "\nCopying $srcdir_rootfs ---> $mnt_dir/mmcblk0p2 (this may take a few minutes)..."
rm -rf $mnt_dir/mmcblk0p2/*
mount -t nfs -o nolock,vers=3,ro $srcdir_rootfs "$mnt_dir/rtfs"
cp -a $mnt_dir/rtfs/* $mnt_dir/mmcblk0p2
sync
umount $mnt_dir/rtfs
printf " done\n"

printf "\nCopying $srcdir_home ---> $mnt_dir/mmcblk0p3 ..."
rm -rf $mnt_dir/mmcblk0p3/*
mount -t nfs -o nolock,vers=3,ro $srcdir_home "$mnt_dir/home"
cp -a $mnt_dir/home/root $mnt_dir/mmcblk0p3
sync
umount $mnt_dir/home
printf " done\n"

printf "\nCopying $srcdir_fw_sw ---> $mnt_dir/mmcblk0p4 ..."
rm -rf $mnt_dir/mmcblk0p4/*
mount -t nfs -o nolock,vers=3,ro $srcdir_fw_sw "$mnt_dir/fwsw"
cp -a $mnt_dir/fwsw/* $mnt_dir/mmcblk0p4
sync
umount $mnt_dir/fwsw
printf " done\n"

# Update fstab
printf "\nRemember to update $mnt_dir/mmcblk0p2/etc/fstab with the following lines:\n"
printf "/dev/mmcblk0p3           /home                auto       defaults,sync,noauto,noatime        0  0\n"
printf "/dev/mmcblk0p4           /fw_sw               auto       defaults,sync,noauto,noatime        0  0\n"
printf "\nSD-Card Setup DONE\n"
