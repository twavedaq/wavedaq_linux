# Setup for DCB SD Card boot

## Partitions

The following partitions have to be made for specific system components/files:

| Partition | Type   | Content                      | Size                   |
|-----------|--------|------------------------------|-----------------------:|
| p1        | vfat32 | BOOT.bin and uEnv.txt        |                 511MiB |
| p2        | ext4   | Root file system (read-only) |                   8GiB |
| p3        | ext4   | /home directories            |                   3GiB |
| p4        | ext4   | /fw_sw directory             | remaining space 3.3GiB |

### How to setup the partitions

The procedure below can also be done by sourcing the script [script/setup_sd_card.sh](script/setup_sd_card.sh). A dump of the partition table can be found in [script/sd_card_partition_table](script/sd_card_partition_table).
Warning: the script should be used with care in order not to damage the system configuration and make the system unbootable.

This is the partitioning procedure when doing the partitions on the DCB itself. Paths might be different when doing the partitioning on a different system.

```
$ cd /mnt
```

Copy the partition table [script/sd_card_partition_table](script/sd_card_partition_table) to /home/root and configure it by doing:

```
$ sfdisk /dev/mmcblk0 < /home/root/sd_card_partition_table
```

If the partition table file is not available, the partitioning can also be done manually. Please refer to the Manual Partitioning section below.

Check the partition table:

```
$ sfdisk -l /dev/mmcblk0
```

Unmount the original single partition, format and mount the new partitions:

```
$ umount /run/media/mmcblk0p1/
$ partprobe /dev/mmcblk0
$ mkfs.vfat /dev/mmcblk0p1
$ mkfs.ext4 /dev/mmcblk0p2
$ mkfs.ext4 /dev/mmcblk0p3
$ mkfs.ext4 /dev/mmcblk0p4
$ mkdir p1 p2 p3 p4 nfs
$ mount /dev/mmcblk0p1 p1
$ mount /dev/mmcblk0p2 p2
$ mount /dev/mmcblk0p3 p3
$ mount /dev/mmcblk0p3 p4
```

The full procedure shown only applies if the DCB is currently configured and bootet via NFS. The IP address has to correspond to your NFS server. If NFS is not available, the contents of the partitions have to be copied from another source. Archives (.tar.gz) can be unpacked into the corresponding partition.

NFS mount:

```
$ mount -t nfs -o nolock,ro,vers=3 129.129.188.245:/data/export/rootfs nfs
```

Copy the root file system:

```
$ cp -a nfs/* p2
$ sync
```

Copy the /home directory:

```
$ cp -a /home/root/ p3
$ sync
```

Copy the /fw_sw directory:

```
$ cp -a /fw_sw/* p4
$ sync
```

Copy BOOT.bin and uEnv.txt:

```
$ cp /boot/BOOT.bin /boot/uEnv.txt /mnt/p1
```

Fstab modifications:

```
$ cd p2/etc
$ nano fstab
```

Edit fstab to look similar to this:

```
/dev/root                /                    auto       ro                                  1  0
proc                     /proc                proc       defaults                            0  0
devpts                   /dev/pts             devpts     mode=0620,gid=5                     0  0
tmpfs                    /run                 tmpfs      mode=0755,nodev,nosuid,strictatime  0  0
tmpfs                    /var/volatile        tmpfs      defaults                            0  0
/dev/mmcblk0p3           /home                auto       defaults,sync,noauto,noatime        0  0
/dev/mmcblk0p4           /fw_sw               auto       defaults,sync,noauto,noatime        0  0
```

Unmount the partitions:

```
$ umount /mnt/p1
$ umount /mnt/p2
$ umount /mnt/p3
$ umount /mnt/p4
```

When booting from the sd-card for the first time, the environment has to be initialized by loading uEnv.txt and configuring hostname and MAC addresses with `dcbcfg <dcb_number>` under u-boot. See u-boot-environment.md for details.

## Hints

To check the currently mounted drives/partitions use `findmnt`

## Manual Partitioning

To partition the SD-card manually do the following within fdisk (output is shown in this part). Commands to be entered are preceeded with ">>>" for readability. Note that "Enter" can be pressed when the default value is accepted.

```
$ fdisk /dev/mmcblk0

>>> Command (m for help): p
Disk /dev/mmcblk0: 14.9 GiB, 15931539456 bytes, 31116288 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device         Boot Start      End  Sectors  Size Id Type
/dev/mmcblk0p1       8192 31116287 31108096 14.9G  c W95 FAT32 (LBA)

>>> Command (m for help): d
Selected partition 1
Partition 1 has been deleted.

>>> Command (m for help): n
Partition type
    p   primary (0 primary, 0 extended, 4 free)
    e   extended (container for logical partitions)
>>> Select (default p): p
>>> Partition number (1-4, default 1):
>>> First sector (2048-31116287, default 2048):
>>> Last sector, +sectors or +size{K,M,G,T,P} (2048-31116287, default 31116287): +511M

Created a new partition 1 of type 'Linux' and of size 511 MiB.

>>> Command (m for help): t
Selected partition 1
>>> Hex code (type L to list all codes): c
Changed type of partition 'Linux' to 'W95 FAT32 (LBA)'.

>>> Command (m for help): n
Partition type
    p   primary (1 primary, 0 extended, 3 free)
    e   extended (container for logical partitions)
>>> Select (default p): p
>>> Partition number (2-4, default 2):
>>> First sector (1048576-31116287, default 1048576):
>>> Last sector, +sectors or +size{K,M,G,T,P} (1048576-31116287, default 31116287): +8G

Created a new partition 2 of type 'Linux' and of size 8 GiB.

>>> Command (m for help): n
Partition type
    p   primary (2 primary, 0 extended, 2 free)
    e   extended (container for logical partitions)
>>> Select (default p): p
>>> Partition number (3,4, default 3):
>>> First sector (17825792-31116287, default 17825792):
>>> Last sector, +sectors or +size{K,M,G,T,P} (17825792-31116287, default 31116287): +3G

Created a new partition 3 of type 'Linux' and of size 3 GiB.

>>> Command (m for help): n
Partition type
    p   primary (2 primary, 0 extended, 2 free)
    e   extended (container for logical partitions)
>>> Select (default p): p
>>> Partition number (4, default 4):
>>> First sector (17825792-31116287, default 17825792):
>>> Last sector, +sectors or +size{K,M,G,T,P} (17825792-31116287, default 31116287):

Created a new partition 4 of type 'Linux' and of size 3.3 GiB.

>>> Command (m for help): p
Disk /dev/mmcblk0: 14.9 GiB, 15931539456 bytes, 31116288 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device         Boot    Start      End  Sectors  Size Id Type
/dev/mmcblk0p1          2048  1048575  1046528  511M  c W95 FAT32 (LBA)
/dev/mmcblk0p2       1048576 17825791 16777216    8G 83 Linux
/dev/mmcblk0p3      17825792 24117247  6291456    3G 83 Linux
/dev/mmcblk0p4      24117248 31116287  6999040  3.3G 83 Linux

>>> Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Re-reading the partition table failed.: Device or resource busy

The kernel still uses the old table. The new table will be used at the next reboot or after you
run partprobe(8) or kpartx(8).
```

Unmount the original single partition, format and mount the new partitions:

```
$ umount /run/media/mmcblk0p1/
$ partprobe /dev/mmcblk0
```

The full procedure shown only applies if the DCB is currently configured and bootet via NFS. The IP address has to correspond to your NFS server. If NFS is not available, the contents of the partitions have to be copied from another source. Archives (.tar.gz) can be unpacked into the corresponding partition.
