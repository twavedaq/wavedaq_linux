# Changes to rootfs-image

The following adaptions in the rootfs are not done by yocto so they have to be done manually.
Some of them might be added to the yocto configuration in the future. Others are intentionally omitted for security reasons.
However yocto was configured to adapt the rootfs as much as possible to the final rootfs state only requiring a minimum of manual adaptions when commissioning a fresh build for operation.

## User settings

The following steps are recommended to set up the user accounts:

* Login as root
  * Set root password with `passwd`
  * Set password for user meg with `passwd meg`
  * If required, create other users and groups in `/etc/passwd` and `/etc/group`
  * Add users to sudoers in `/etc/sudoers`


## User drives mount settings

Set the drives to mount in `/etc/fstab`.
The following folders should be mounted from SD-card or NFS:
* `/home` containing the user folders
* `/fw_sw` containing firmware and software binaries

The most common mount configurations for MEG are already in `/etc/fstab` as comments.
I.e. all that has to be done for a MEG configuration is to comment certain lines in or out.


## SSH Keys

Copy ssh keys and config to `/etc/ssh`.

Note that etc/ssh/sshd_config_readonly might contain paths to keys in /var/run or /run.
Since these are temporary filesystems, they might have to be replaced.
Usually the same paths to the keys can be used as in etc/ssh/sshd_config in case the keys are present there.

The configuration in etc/ssh/sshd_config_readonly contains the configuration to be used by the SSH daemon if
rootfs is read-only which is usually the case if it is mounted through NFS.

## Adjust Services as Required:

Services are already optimized in the yocto build i.e. many unused services are removed from autostart
in the recipe layer/meta-psi-wavedaq/meta-wavedaq-dcb-bsp/recipes-bsp/rootfs/core-image-lsb-sdk.bbappend.
However if there are still unwanted daemons started during boot, they should be removed with
```
$ chkconfig <options>
```



# Add files to /etc/yum.repos.d/

## /etc/yum.repos.d/ems-rh7-lc-noarch.repo

```
[ems-rh7-lc-noarch]
name=yocto deploy rpm noarch
baseurl=file:///mnt/ems-rh7-lc/repo/system/dcb_rootfs_lsb/deploy/rpm/noarch
enabled=1
metadata_expire=0
gpgcheck=0
```


## /etc/yum.repos.d/ems-rh7-lc-cortexa9t2hf-neon.repo

```
[ems-rh7-lc]
name=yocto deploy rpm cortexa9t2hf_neon
baseurl=file:///mnt/ems-rh7-lc/repo/system/dcb_rootfs_lsb/deploy/rpm/cortexa9t2hf_neon
enabled=1
metadata_expire=0
gpgcheck=0
```



# Optional GUI Tools

## geany (Text-Editor)

On DCB:

```
$ wget http://download-ib01.fedoraproject.org/pub/fedora-secondary/releases/30/Everything/i386/os/Packages/g/gnome-icon-theme-3.12.0-11.fc30.noarch.rpm

$ wget http://download-ib01.fedoraproject.org/pub/fedora-secondary/releases/30/Everything/i386/os/Packages/g/gnome-icon-theme-symbolic-3.12.0-9.fc30.noarch.rpm

$ sudo rpm -i gnome-icon-theme-symbolic-3.12.0-9.fc30.noarch.rpm
$ sudo rpm -i gnome-icon-theme-3.12.0-11.fc30.noarch.rpm

$ sudo cp  /usr/share/icons/hicolor/16x16/actions/meld* /usr/share/icons/gnome/16x16/actions/
$ sudo gtk-update-icon-cache /usr/share/icons/gnome
```


# meld (Diff-Tool)

Requires package xmlto for building and others at runtime

on ems-rh7-lc:

```
$ bitbake xmlto
$ bitbake gtksourceview3
$ bitbake gsettings-desktop-schemas
$ bitbake package-index
```

on DCB: (after repository is properly configured)

```
$ dnf install xmlto gtksourceview3 gsettings-desktop-schemas
$ git clone https://gitlab.gnome.org/GNOME/meld.git
$ cd meld
$ git checkout 9b4f890f96cbbf39dac6bda3ad438128e51f8558
$ python3 setup.py build
$ python3 setup.py --no-update-icon-cache --no-compile-schemas install --prefix=/usr --root=./install --optimize=1
$ sudo cp -r install/usr /
```

# Optional Shell Tools:

## i2c-tools

Can be used to test I2C links from the shell.

On ems-rh7-lc:

```
$ bitbake i2c-tools
$ bitbake package-index
```

On DCB: (after repository is properly configured)

```
$ dnf install i2c-tools
```
---------------------------------------------------------------------------
